import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: () => import('./components/pages/main/Main.vue')
    },

    {
      path: '/about',
      name: 'about',
      component: () => import('./components/pages/About.vue')
    },

    {
      path: '/prices',
      redirect: '/prices/consult',
    },

    {
      path: '/prices/:special',
      name: 'prices', 
      component: () => import('./components/pages/prices/Prices.vue')
    },

    {
      path: '/crew',
      redirect: '/crew/pediatrist'
    },

    {
      path: '/crew/:special',
      name: 'crew',
      component: () => import('./components/pages/crew/Crew.vue')

    },

    {
      path: '/contacts',
      component: () => import('./components/pages/Contacts.vue'),
    },

    {
      path: '/programs',
      component: () => import('./components/pages/programs/Programs.vue')
    },

    {
      path: '/complexes',
      component: () => import('./components/pages/complexes/Complexes.vue')
    }
  ],
  mode: 'history',
})
