export default [{
    name: 'Белякова Светлана Васильевна',
    rank: '',
    experience: '13 лет стажа',
    img: '11',
  },

  {
    name: 'Бердникова Анна Гересовна',
    rank: 'Высшая категория',
    experience: '20+ лет стажа',
    img: '12'
  },

  {
    name: 'Захарова Елена Владимировна',
    rank: 'Высшая категория',
    experience: '20+ лет стажа',
    img: '13'
  },

  {
    name: 'Никитина Наталья Анатольевна',
    rank: 'Высшая категория',
    experience: '25+ лет стажа',
    img: '14'
  },

  {
    name: 'Александрова Любовь Сергеевна',
    rank: 'Первая категория',
    experience: '8 лет стажа',
    img: '27'
  },

  {
    name: 'Сергеева Анастасия Валерьевна',
    rank: 'Первая категория',
    experience: '16 лет стажа',
    img: '34'
  },
]